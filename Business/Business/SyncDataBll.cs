﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Business.Cache;
using Business.SystemManage;
using Cache.Factory;
using Entity;
using Entity.OrganizationManage;
using Entity.SystemManage;
using Enum;
using Enum.OrganizationManage;
using Model;
using Model.Param;
using Model.Param.OrganizationManage;
using Service;
using Service.OrganizationManage;
using Util;
using Util.Extension;
using Util.Model;
using Web.Code;

namespace Business
{
    public class SyncDataBll
    {
        private SyncDataService syncDataService = new SyncDataService();
        #region
        public async Task<TData> syncMetroTypeList()
        {
            await syncDataService.sync_metro_typelist();
            return new TData();
        }
        #endregion
    }
}
