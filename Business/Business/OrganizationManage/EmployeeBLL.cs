﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Util;
using Util.Extension;
using Util.Model;
using Entity.OrganizationManage;
using Model.Param.OrganizationManage;
using Service.OrganizationManage;
using Model.Param;

namespace Business.OrganizationManage
{
    /// <summary>
    /// 创 建：admin
    /// 日 期：2021-05-17 16:19
    /// 描 述：员工信息业务类
    /// </summary>
    public class EmployeeBLL
    {
        private EmployeeService employeeService = new EmployeeService();
        private EmployeePartTimeService employeePartTimeService = new EmployeePartTimeService();
        private DepartmentService departmentService = new DepartmentService();
        private JobService jobService = new JobService();
      
        #region 获取数据
        public async Task<TData<List<EmployeeEntity>>> GetList(EmployeeListParam param)
        {
            TData<List<EmployeeEntity>> obj = new TData<List<EmployeeEntity>>();
            obj.Data = await employeeService.GetList(param);
            obj.Total = obj.Data.Count;
            obj.Tag = 1;
            return obj;
        }
        public async Task<TData<List<EmployeePartTimeListVM>>> GetEmpPartTimeList(long EmpId) 
        {
            TData<List<EmployeePartTimeListVM>> obj = new TData<List<EmployeePartTimeListVM>>();
            obj.Data = await employeePartTimeService.GetEmpPartTimeList(EmpId);
            obj.Total = obj.Data.Count;
            obj.Tag = 1;
            return obj;
        }
        public async Task<TData<List<EmployeeEntity>>> GetPageList(EmployeeListParam param, Pagination pagination)
        {
            TData<List<EmployeeEntity>> obj = new TData<List<EmployeeEntity>>();
            obj.Data = await employeeService.GetPageList(param, pagination);
            obj.Total = pagination.TotalCount;
            obj.Tag = 1;
            return obj;
        }

        public async Task<TData<EmployeeEntity>> GetEntity(long id)
        {
            TData<EmployeeEntity> obj = new TData<EmployeeEntity>();
            obj.Data = await employeeService.GetEntity(id);
            if (obj.Data != null)
            {
                var empPartTimeEntity = employeePartTimeService.GetEntity(id, 0);
                if (empPartTimeEntity.Result != null)
                {
                    obj.Data.DeptId = empPartTimeEntity.Result.DeptId;
                    obj.Data.JobId = empPartTimeEntity.Result.JobId;
                }
                obj.Tag = 1;
            }
            return obj;
        }
        public async Task<TData<EmployeePartTimeEntity>> GetPartTimeEntity(long id)
        {
            TData<EmployeePartTimeEntity> obj = new TData<EmployeePartTimeEntity>();
            obj.Data = await employeePartTimeService.GetEntity(0,id);
            if (obj.Data != null)
            {
                obj.Tag = 1;
            }
            return obj;
        }
        #endregion

        #region 提交数据
        public async Task<TData<string>> SaveForm(EmployeeEntity entity)
        {
            TData<string> obj = new TData<string>();
            //部门-岗位关系
            var empPartTimeEntity = new EmployeePartTimeEntity()
            {
                DeptId = entity.DeptId.Value,
                JobId = entity.JobId.Value,
                Type = entity.IsPartTime.Value
            };
            if (!entity.Id.IsNullOrZero())
            {
                empPartTimeEntity.EmpId = entity.Id.Value;
                var GetEmpPartEntity = employeePartTimeService.GetEntity(entity.Id.Value,0);
                if (GetEmpPartEntity.Result != null) 
                {
                    empPartTimeEntity.Id = GetEmpPartEntity.Result.Id;
                    if (employeePartTimeService.ExistPartTime(empPartTimeEntity))
                    {
                        obj.Message = "该部门岗位已经关联";
                        return obj;
                    }
                }
               
            }
            await employeeService.SaveForm(entity);
            empPartTimeEntity.EmpId = entity.Id.Value;
           
            await employeePartTimeService.SaveEmployeePartTimeForm(empPartTimeEntity);
            obj.Data = entity.Id.ParseToString();
            obj.Tag = 1;
            return obj;
        }
        /// <summary>
        /// 保存兼职数据
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<TData<string>> SavePartTimeForm(EmployeePartTimeEntity entity)
        {
            TData<string> obj = new TData<string>();
            if (employeePartTimeService.ExistPartTime(entity))
            {
                obj.Message = "该部门岗位已经关联";
                return obj;
            }
            //部门-岗位关系
            await employeePartTimeService.SaveEmployeePartTimeForm(entity);
            obj.Data = entity.Id.ParseToString();
            obj.Tag = 1;
            return obj;
        }

        public async Task<TData> DeleteForm(string ids)
        {
            TData obj = new TData();
            await employeeService.DeleteForm(ids);
            obj.Tag = 1;
            return obj;
        }
        public async Task<TData> ImportEmployee(ImportParam param, List<EmployeeImportEntity> list)
        {
            TData obj = new TData();
            if (list.Any())
            {
                foreach (EmployeeImportEntity entity in list)
                {
                    var deptEntity = departmentService.GetEntity(entity.ParentDepartment);
                    if (deptEntity == null) 
                    {
                        //deptEntity.Result.DepartmentName = entity.ParentDepartment;
                        //deptEntity.Result.
                    }

                    //EmployeeEntity dbEntity = await employeeService.GetEntity(entity.EmpWorkID);
                    //if (dbEntity != null)
                    //{
                    //    entity.Id = dbEntity.Id;
                    //    if (param.IsOverride == 1)
                    //    {
                    //        await userService.SaveForm(entity);
                    //        await RemoveCacheById(entity.Id.Value);
                    //    }
                    //}
                    //else
                    //{
                    //    await userService.SaveForm(entity);
                    //    await RemoveCacheById(entity.Id.Value);
                    //}
                }
                obj.Tag = 1;
            }
            else
            {
                obj.Message = " 未找到导入的数据";
            }
            return obj;
        }
        #endregion

        #region 私有方法
        #endregion
    }
}
