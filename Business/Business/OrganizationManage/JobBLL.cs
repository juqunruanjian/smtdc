﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity.OrganizationManage;
using Enum.OrganizationManage;
using Model;
using Model.Result;
using Model.Param.OrganizationManage;
using Service.OrganizationManage;
using Util;
using Util.Model;
using Util.Extension;
using Web.Code;
using Business.Cache;
using Entity.SystemManage;
using Enum.SystemManage;
using Service.SystemManage;
namespace Business.OrganizationManage
{
    /// <summary>
    /// 创 建：admin
    /// 日 期：2021-05-18 14:26
    /// 描 述：岗位信息业务类
    /// </summary>
    public class JobBLL
    {
        private JobService jobService = new JobService();
        private UserService userService = new UserService();
        private DepartmentService deptService = new DepartmentService();
        private MenuAuthorizeCache menuAuthorizeCache = new MenuAuthorizeCache();
        private MenuAuthorizeService menuAuthorizeService = new MenuAuthorizeService();
        #region 获取数据
        public async Task<TData<List<JobEntity>>> GetList(JobListParam param)
        {
            TData<List<JobEntity>> obj = new TData<List<JobEntity>>();
            obj.Data = await jobService.GetList(param);
            foreach (JobEntity entity in obj.Data)
            {
                var dep = deptService.GetEntity(entity.JobDeptID.Value).Result;
                if (dep != null)
                {
                    entity.DeptName = dep.DepartmentName;
                }
            }

            obj.Tag = 1;
            return obj;
        }

        public async Task<TData<List<ZtreeInfo>>> GetZtreeJobList(long depId=0)
        {
            
            var obj = new TData<List<ZtreeInfo>>();
            obj.Data = new List<ZtreeInfo>();
            var param = new JobListParam()
            {
                DepId = depId
            };
            if (depId == 0) 
            {
                param = new JobListParam();
            }
            List<JobEntity> jobList = await jobService.GetList(param);
            foreach (JobEntity job in jobList)
            {
                obj.Data.Add(new ZtreeInfo
                {
                    id = job.Id,
                    pId = job.ParentId,
                    name = job.JobName
                });
            }
            obj.Tag = 1;
            return obj;
        }

        public async Task<TData<List<ZtreeInfo>>> GetZtreeUserList(JobListParam param)
        {
            var obj = new TData<List<ZtreeInfo>>();
            obj.Data = new List<ZtreeInfo>();
            List<JobEntity> jobList = await jobService.GetList(param);
            OperatorInfo operatorInfo = await Operator.Instance.Current();

            List<UserEntity> userList = await userService.GetList(null);
            foreach (UserEntity user in userList)
            {
                obj.Data.Add(new ZtreeInfo
                {
                    id = user.Id,

                    name = user.RealName
                });
            }
            obj.Tag = 1;
            return obj;
        }

        public async Task<TData<JobEntity>> GetEntity(long id)
        {
            TData<JobEntity> obj = new TData<JobEntity>();
            JobEntity jobEntity = await jobService.GetEntity(id);
            List<MenuAuthorizeEntity> menuAuthorizeList = await menuAuthorizeService.GetList(new MenuAuthorizeEntity
            {
                AuthorizeId = id,
                AuthorizeType = AuthorizeTypeEnum.Job.ParseToInt()
            });
            // 获取角色对应的权限
            jobEntity.MenuIds = string.Join(",", menuAuthorizeList.Select(p => p.MenuId));
            obj.Data = jobEntity;
            obj.Tag = 1;
            return obj;
          
           
        }

        public async Task<TData<int>> GetMaxSort()
        {
            TData<int> obj = new TData<int>();
            obj.Data = await jobService.GetMaxSort();
            obj.Tag = 1;
            return obj;
        }
        public async Task<TData<List<ZtreeInfo>>> GetListByDept(long deptId) 
        {
            var obj = new TData<List<ZtreeInfo>>();
            obj.Data = new List<ZtreeInfo>();
           
            List<JobEntity> jobList = await jobService.GetListByDept(deptId);
            foreach (JobEntity job in jobList)
            {
                obj.Data.Add(new ZtreeInfo
                {
                    id = job.Id,
                    pId = job.ParentId,
                    name = job.JobName
                });
            }
            obj.Tag = 1;
            return obj;
        }
        #endregion

        #region 提交数据
        public async Task<TData<string>> SaveForm(JobEntity entity)
        {
            TData<string> obj = new TData<string>();
            if (!entity.Id.IsNullOrZero() && entity.Id == entity.ParentId)
            {
                obj.Message = "不能选择自己作为上级岗位！";
                return obj;
            }
            if (jobService.ExistJobtName(entity))
            {
                obj.Message = "岗位名称已经存在！";
                return obj;
            }
            //if (entity.JobDeptID.IsNullOrZero()) 
            //{
            //    obj.Message = "部门信息不能为空！";
            //    return obj;
            //} 
            // 清除缓存里面的权限数据
            menuAuthorizeCache.Remove();
            await jobService.SaveForm(entity);
            obj.Data = entity.Id.ParseToString();
            obj.Tag = 1;
            return obj;
        }

        public async Task<TData> DeleteForm(string ids)
        {
            TData obj = new TData();
            foreach (long id in TextHelper.SplitToArray<long>(ids, ','))
            {
                if (jobService.ExistChildrenJob(id))
                {
                    obj.Message = "该岗位下面有子岗位！";
                    return obj;
                }
            }
            await jobService.DeleteForm(ids);
            obj.Tag = 1;
            return obj;
        }
        #endregion

        #region 公共方法
        /// <summary>
        /// 获取当前岗位及下面所有的岗位
        /// </summary>
        /// <param name="departmentList"></param>
        /// <param name="departmentId"></param>
        /// <returns></returns>
        public async Task<List<long>> GetChildrenJobIdList(List<JobEntity> jobList, long jobId)
        {
            if (jobList == null)
            {
                jobList = await jobService.GetList(null);
            }
            List<long> jobListIdList = new List<long>();
            jobListIdList.Add(jobId);
            GetChildrenJobIdList(jobList, jobId, jobListIdList);
            return jobListIdList;
        }
        #endregion 

        #region 私有方法
        /// <summary>
        /// 获取该岗位下面所有的子岗位
        /// </summary>
        /// <param name="departmentList"></param>
        /// <param name="departmentId"></param>
        /// <param name="departmentIdList"></param>
        private void GetChildrenJobIdList(List<JobEntity> jobList, long jobId, List<long> jobIdList)
        {
            var children = jobList.Where(p => p.ParentId == jobId).Select(p => p.Id.Value).ToList();
            if (children.Count > 0)
            {
                jobIdList.AddRange(children);
                foreach (long id in children)
                {
                    GetChildrenJobIdList(jobList, id, jobIdList);
                }
            }
        }
        #endregion
    }
}
