﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Util;
using Util.Extension;
using Util.Model;
using Entity.SystemManage;
using Model.Param.SystemManage;
using Service.SystemManage;
using Model.Result;

namespace Business.SystemManage
{
    /// <summary>
    /// 创 建：admin
    /// 日 期：2021-06-29 11:23
    /// 描 述：业务类
    /// </summary>
    public class TypeListBLL
    {
        private TypeListService typeListService = new TypeListService();

        #region 获取数据
        public async Task<TData<List<TypeListEntity>>> GetList(TypeListListParam param)
        {
            TData<List<TypeListEntity>> obj = new TData<List<TypeListEntity>>();
            obj.Data = await typeListService.GetList(param);
            obj.Total = obj.Data.Count;
            obj.Tag = 1;
            return obj;
        }
        public async Task<TData<List<ZtreeInfo>>> GetZtreeTypeList(TypeListListParam param)
        {
            var obj = new TData<List<ZtreeInfo>>();
            obj.Data = new List<ZtreeInfo>();
            List<TypeListEntity> typeList = await typeListService.GetList(param);
            foreach (TypeListEntity type in typeList)
            {
                obj.Data.Add(new ZtreeInfo
                {
                    id = type.Id,
                    pId = type.ParentId,
                    name = type.Name
                });
            }
            obj.Tag = 1;
            return obj;
        }
        public async Task<TData<List<TypeListEntity>>> GetPageList(TypeListListParam param, Pagination pagination)
        {
            TData<List<TypeListEntity>> obj = new TData<List<TypeListEntity>>();
            obj.Data = await typeListService.GetPageList(param, pagination);
            obj.Total = pagination.TotalCount;
            obj.Tag = 1;
            return obj;
        }

        public async Task<TData<TypeListEntity>> GetEntity(long id)
        {
            TData<TypeListEntity> obj = new TData<TypeListEntity>();
            obj.Data = await typeListService.GetEntity(id);
            if (obj.Data != null)
            {
                obj.Tag = 1;
            }
            return obj;
        }
        #endregion

        #region 提交数据
        public async Task<TData<string>> SaveForm(TypeListEntity entity)
        {
            TData<string> obj = new TData<string>();
            await typeListService.SaveForm(entity);
            obj.Data = entity.Id.ParseToString();
            obj.Tag = 1;
            return obj;
        }

        public async Task<TData> DeleteForm(string ids)
        {
            TData obj = new TData();
            await typeListService.DeleteForm(ids);
            obj.Tag = 1;
            return obj;
        }
        #endregion

        #region 私有方法
        #endregion
    }
}
