﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Business.Cache;
using Business.OrganizationManage;
using Entity.OrganizationManage;
using Entity.SystemManage;
using Enum;
using Enum.SystemManage;
using Model.Result;
using Service.SystemManage;
using Util.Extension;
using Util.Model;
using Web.Code;

namespace Business.SystemManage
{
    public class MenuAuthorizeBLL
    {
        private MenuAuthorizeCache menuAuthorizeCache = new MenuAuthorizeCache();
        private MenuCache menuCache = new MenuCache();

        #region 获取数据
        public async Task<TData<List<MenuAuthorizeInfo>>> GetAuthorizeList(OperatorInfo user)
        {
            TData<List<MenuAuthorizeInfo>> obj = new TData<List<MenuAuthorizeInfo>>();
            obj.Data = new List<MenuAuthorizeInfo>();

            List<MenuAuthorizeEntity> authorizeList = new List<MenuAuthorizeEntity>();
            List<MenuAuthorizeEntity> userAuthorizeList = null;
            List<MenuAuthorizeEntity> roleAuthorizeList = null;
            List<MenuAuthorizeEntity> deptAuthorizeList = null;
            List<MenuAuthorizeEntity> jobAuthorizeList = null;
            var menuAuthorizeCacheList = await menuAuthorizeCache.GetList();
            var menuList = await menuCache.GetList();
            var enableMenuIdList = menuList.Where(p => p.MenuStatus == (int)StatusEnum.Yes).Select(p => p.Id).ToList();

            menuAuthorizeCacheList = menuAuthorizeCacheList.Where(p => enableMenuIdList.Contains(p.MenuId)).ToList();

            // 用户
            userAuthorizeList = menuAuthorizeCacheList.Where(p => p.AuthorizeId == user.UserId && p.AuthorizeType == AuthorizeTypeEnum.User.ParseToInt()).ToList();

            // 角色
            if (user.RoleInfo!=null)
            {
                List<long> roleIdList = user.RoleInfo.Select(c => c.RoleId).ToList();
                roleAuthorizeList = menuAuthorizeCacheList.Where(p => roleIdList.Contains(p.AuthorizeId.Value) && p.AuthorizeType == AuthorizeTypeEnum.Role.ParseToInt()).ToList();
            }
            //获取部门关联的权限
            if (user.DeptInfo!=null) 
            {
                List<long> deptIdList = user.DeptInfo.Select(c => c.DeptId).ToList();
                deptAuthorizeList = menuAuthorizeCacheList.Where(p => deptIdList.Contains(p.AuthorizeId.Value) && p.AuthorizeType == AuthorizeTypeEnum.Dept.ParseToInt()).ToList();
            }
            //获取岗位关联的权限
            if (user.JobInfo!=null)
            {
                List<long> jobIdList = user.JobInfo.Select(c => c.JobId).ToList();
                jobAuthorizeList = menuAuthorizeCacheList.Where(p => jobIdList.Contains(p.AuthorizeId.Value) && p.AuthorizeType == AuthorizeTypeEnum.Job.ParseToInt()).ToList();
            }
            // 排除重复的记录
            if (userAuthorizeList.Count > 0)
            {
                authorizeList.AddRange(userAuthorizeList);
                roleAuthorizeList = roleAuthorizeList.Where(p => !userAuthorizeList.Select(u => u.AuthorizeId).Contains(p.AuthorizeId)).ToList();
            }
            if (roleAuthorizeList != null && roleAuthorizeList.Count > 0)
            {
                authorizeList.AddRange(roleAuthorizeList);
            }
            if (deptAuthorizeList != null && deptAuthorizeList.Count > 0)
            {
                authorizeList.AddRange(deptAuthorizeList);
            }
            if (jobAuthorizeList != null && jobAuthorizeList.Count > 0)
            {
                authorizeList.AddRange(jobAuthorizeList);
            }
            foreach (MenuAuthorizeEntity authorize in authorizeList)
            {
                obj.Data.Add(new MenuAuthorizeInfo
                {
                    MenuId = authorize.MenuId,
                    AuthorizeId = authorize.AuthorizeId,
                    AuthorizeType = authorize.AuthorizeType,
                    Authorize = menuList.Where(t => t.Id == authorize.MenuId).Select(t => t.Authorize).FirstOrDefault()
                });
            }
            obj.Tag = 1;
            return obj;
        }
        #endregion
    }
}
