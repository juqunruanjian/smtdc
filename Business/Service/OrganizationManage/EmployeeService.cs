﻿using System;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Linq.Expressions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Util;
using Util.Extension;
using Util.Model;
using Data;
using Data.Repository;
using Entity.OrganizationManage;
using Model.Param.OrganizationManage;

namespace Service.OrganizationManage
{
    /// <summary>
    /// 创 建：admin
    /// 日 期：2021-05-17 16:19
    /// 描 述：员工信息服务类
    /// </summary>
    public class EmployeeService :  RepositoryFactory
    {
        #region 获取数据
        public async Task<List<EmployeeEntity>> GetList(EmployeeListParam param)
        {
            //var expression = ListFilter(param);
            //var list = await this.BaseRepository().FindList(expression);
            //return list.ToList();
            return null;
        }

        public async Task<List<EmployeeEntity>> GetPageList(EmployeeListParam param, Pagination pagination)
        {
            var strSql = new StringBuilder();

            List<DbParameter> filter = ListFilter(param, strSql);
            var list = await this.BaseRepository().FindList<EmployeeEntity>(strSql.ToString(), filter.ToArray(), pagination);
            return list.ToList();
        }

        public async Task<EmployeeEntity> GetEntity(long id)
        {
            return await this.BaseRepository().FindEntity<EmployeeEntity>(id);
        }
        public async Task<EmployeeEntity> GetEntity(string idNumber)
        {
            return await this.BaseRepository().FindEntity<EmployeeEntity>(p => p.EmpWorkID == idNumber);
        }
        #endregion

        #region 提交数据
        public async Task SaveForm(EmployeeEntity entity)
        {
            if (entity.Id.IsNullOrZero())
            {
                entity.Create();
                await this.BaseRepository().Insert(entity);
            }
            else
            {
                
                await this.BaseRepository().Update(entity);
            }
        }

        public async Task DeleteForm(string ids)
        {
            long[] idArr = TextHelper.SplitToArray<long>(ids, ',');
            await this.BaseRepository().Delete<EmployeeEntity>(idArr);
        }
        #endregion

        #region 私有方法
        private List<DbParameter> ListFilter(EmployeeListParam param,StringBuilder strSql)
        {
            strSql.Append("Select a.* From SysEmployee a ");
            var parameter = new List<DbParameter>();
            if (param != null) 
            {
                
                if (param.DeptId.HasValue || param.JobId.HasValue)
                {
                    strSql.Append("Left Join SysEmployeePartTime b on a.Id=b.EmpId Where 1=1 ");
                    if (param.DeptId.HasValue) 
                    {
                        strSql.Append(" And b.DeptId=@DeptId");
                        parameter.Add(DbParameterExtension.CreateDbParameter("@DeptId", param.DeptId));
                    }
                    if (param.JobId.HasValue) 
                    {
                        strSql.Append(" And b.JobId=@JobId");
                        parameter.Add(DbParameterExtension.CreateDbParameter("@JobId", param.JobId));
                    }
                }
                
            }
            return parameter;
        }
        #endregion
    }
}
