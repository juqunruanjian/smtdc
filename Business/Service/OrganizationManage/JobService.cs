﻿using System;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Linq.Expressions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Util;
using Util.Extension;
using Util.Model;
using Data;
using Data.Repository;
using Entity.OrganizationManage;
using Model.Param.OrganizationManage;
using Entity.SystemManage;
using Enum.SystemManage;

namespace Service.OrganizationManage
{
    /// <summary>
    /// 创 建：admin
    /// 日 期：2021-05-18 14:26
    /// 描 述：岗位信息服务类
    /// </summary>
    public class JobService :  RepositoryFactory
    {
        #region 获取数据
        public async Task<List<JobEntity>> GetList(JobListParam param)
        {
            var expression = ListFilter(param);
            var list = await this.BaseRepository().FindList(expression);
            return list.OrderBy(p => p.JobSort).ToList();
        }

        public async Task<JobEntity> GetEntity(long id)
        {
            return await this.BaseRepository().FindEntity<JobEntity>(id);
        }
        public async Task<List<JobEntity>> GetListByDept(long deptId) 
        {
            var jobList = await BaseRepository().FindList<JobEntity>("SELECT B.Id,B.JobName FROM SysDeptWithJob A LEFT JOIN SysJob B ON A.JobId=B.Id WHERE A.DeptId='"+deptId+"'");
            return jobList.ToList();

        }
        public async Task<int> GetMaxSort()
        {
            object result = await this.BaseRepository().FindObject("SELECT MAX(JobSort) FROM SysJob");
            int sort = result.ParseToInt();
            sort++;
            return sort;
        }

        /// <summary>
        /// 部门名称是否存在
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public bool ExistJobtName(JobEntity entity)
        {
            var expression = LinqExtensions.True<JobEntity>();
            expression = expression.And(t => t.BaseIsDelete == 0);
            if (entity.Id.IsNullOrZero())
            {
                expression = expression.And(t => t.JobName == entity.JobName&&t.JobDeptID==entity.JobDeptID);
            }
            else
            {
                expression = expression.And(t => t.JobName == entity.JobName && t.Id != entity.Id);
            }
            return this.BaseRepository().IQueryable(expression).Count() > 0 ? true : false;
        }

        /// <summary>
        /// 是否存在子部门
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool ExistChildrenJob(long id)
        {
            var expression = LinqExtensions.True<JobEntity>();
            expression = expression.And(t => t.ParentId == id);
            return this.BaseRepository().IQueryable(expression).Count() > 0 ? true : false;
        }
        #endregion

        #region 提交数据
        public async Task SaveForm(JobEntity entity)
        {
            var db = await this.BaseRepository().BeginTrans();
            try
            {
                if (entity.Id.IsNullOrZero())
                {
                    await entity.Create();
                    await db.Insert(entity);
                }
                else
                {
                    await db.Delete<MenuAuthorizeEntity>(t => t.AuthorizeId == entity.Id);
                    await entity.Modify();
                    await db.Update(entity);
                }
                // 角色对应的菜单、页面和按钮权限
                if (!string.IsNullOrEmpty(entity.MenuIds))
                {
                    foreach (long menuId in TextHelper.SplitToArray<long>(entity.MenuIds, ','))
                    {
                        MenuAuthorizeEntity menuAuthorizeEntity = new MenuAuthorizeEntity();
                        menuAuthorizeEntity.AuthorizeId = entity.Id;
                        menuAuthorizeEntity.MenuId = menuId;
                        menuAuthorizeEntity.AuthorizeType = AuthorizeTypeEnum.Job.ParseToInt();
                        await menuAuthorizeEntity.Create();
                        await db.Insert(menuAuthorizeEntity);
                    }
                }
                await db.CommitTrans();
            }
            catch
            {
                await db.RollbackTrans();
                throw;
            }
        }

        public async Task DeleteForm(string ids)
        {
            var db = await this.BaseRepository().BeginTrans();
            try
            {
                long[] idArr = TextHelper.SplitToArray<long>(ids, ',');
                await db.Delete<JobEntity>(idArr);
                await db.CommitTrans();
            }
            catch
            {
                await db.RollbackTrans();
                throw;
            }
        }
        #endregion

        #region 私有方法
        private Expression<Func<JobEntity, bool>> ListFilter(JobListParam param)
        {
            var expression = LinqExtensions.True<JobEntity>();
            if (param != null)
            {
                if (!param.JobName.IsEmpty())
                {
                    expression = expression.And(t => t.JobName.Contains(param.JobName));
                }
                if (param.DepId.HasValue) 
                {
                    expression = expression.And(t => t.JobDeptID==param.DepId);
                }
                if (!param.JobType.IsNullOrZero()) 
                {
                    expression = expression.And(t => t.JobType == param.JobType);
                }
            }
            return expression;
        }
        #endregion
    }
}
