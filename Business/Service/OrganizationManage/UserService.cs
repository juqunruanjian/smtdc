﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Linq.Expressions;
using System.Data;
using System.Data.Common;
using Data.Repository;
using Entity.OrganizationManage;
using Enum.OrganizationManage;
using Model.Param.OrganizationManage;
using Util;
using Util.Model;
using Util.Extension;
using Enum;
using Entity;
using Data.EF;
using Service.SystemManage;

namespace Service.OrganizationManage
{
    public class UserService : RepositoryFactory
    {
        #region 获取数据
        public async Task<List<UserEntity>> GetList(UserListParam param)
        {
            var expression = ListFilter(param);
            var list = await this.BaseRepository().FindList(expression);
            return list.ToList();
        }

        public async Task<List<UserEntity>> GetPageList(UserListParam param, Pagination pagination)
        {
            var expression = ListFilter(param);
            var list = await this.BaseRepository().FindList(expression, pagination);
            return list.ToList();
        }

        public async Task<UserEntity> GetEntity(long id)
        {
            return await this.BaseRepository().FindEntity<UserEntity>(id);
        }

        public async Task<UserEntity> GetEntity(string userName)
        {
            return await this.BaseRepository().FindEntity<UserEntity>(p => p.UserName == userName);
        }

        public async Task<UserEntity> CheckLogin(string userName)
        {
            var expression = LinqExtensions.True<UserEntity>();
            expression = expression.And(t => t.UserName == userName);
            expression = expression.Or(t => t.Mobile == userName);
            expression = expression.Or(t => t.Email == userName);
            return await this.BaseRepository().FindEntity(expression);
        }

        public bool ExistUserName(UserEntity entity)
        {
            var expression = LinqExtensions.True<UserEntity>();
            expression = expression.And(t => t.BaseIsDelete == 0);
            if (entity.Id.IsNullOrZero())
            {
                expression = expression.And(t => t.UserName == entity.UserName);
            }
            else
            {
                expression = expression.And(t => t.UserName == entity.UserName && t.Id != entity.Id);
            }
            return this.BaseRepository().IQueryable(expression).Count() > 0 ? true : false;
        }
        #endregion

        #region 提交数据
        public async Task UpdateUser(UserEntity entity)
        {
            await this.BaseRepository().Update(entity);
        }

        public async Task SaveForm(UserEntity entity)
        {
            var db = await this.BaseRepository().BeginTrans();
            try
            {
                if (entity.Id.IsNullOrZero())
                {
                    await entity.Create();
                    await db.Insert(entity);
                }
                else
                {
                    await db.Delete<UserBelongEntity>(t => t.ObjectId == entity.Id);

                    // 密码不进行更新，有单独的方法更新密码
                    entity.Password = null;
                    await entity.Modify();
                    await db.Update(entity);
                }
                // 职位
                if (!string.IsNullOrEmpty(entity.PositionIds))
                {
                    foreach (long positionId in TextHelper.SplitToArray<long>(entity.PositionIds, ','))
                    {
                        UserBelongEntity positionBelongEntity = new UserBelongEntity();
                        positionBelongEntity.ObjectId = entity.Id;
                        positionBelongEntity.BelongId = positionId;
                        positionBelongEntity.BelongType = UserBelongTypeEnum.Position.ParseToInt();
                        await positionBelongEntity.Create();
                        await db.Insert(positionBelongEntity);
                    }
                }
                // 角色
                if (!string.IsNullOrEmpty(entity.RoleIds))
                {
                    foreach (long roleId in TextHelper.SplitToArray<long>(entity.RoleIds, ','))
                    {
                        UserBelongEntity departmentBelongEntity = new UserBelongEntity();
                        departmentBelongEntity.ObjectId = entity.Id;
                        departmentBelongEntity.BelongId = roleId;
                        departmentBelongEntity.BelongType = UserBelongTypeEnum.Role.ParseToInt();
                        await departmentBelongEntity.Create();
                        await db.Insert(departmentBelongEntity);
                    }
                }
                await db.CommitTrans();
            }
            catch
            {
                await db.RollbackTrans();
                throw;
            }
        }

        public async Task DeleteForm(string ids)
        {
            var db = await this.BaseRepository().BeginTrans();
            try
            {
                long[] idArr = TextHelper.SplitToArray<long>(ids, ',');
                await db.Delete<UserEntity>(idArr);
                await db.Delete<UserBelongEntity>(t => idArr.Contains(t.ObjectId.Value));
                await db.CommitTrans();
            }
            catch
            {
                await db.RollbackTrans();
                throw;
            }
        }

        public async Task ResetPassword(UserEntity entity)
        {
            await entity.Modify();
            await this.BaseRepository().Update(entity);
        }

        public async Task ChangeUser(UserEntity entity)
        {
            await entity.Modify();
            await this.BaseRepository().Update(entity);
        }
        #endregion

        #region 私有方法
        private Expression<Func<UserEntity, bool>> ListFilter(UserListParam param)
        {
            var expression = LinqExtensions.True<UserEntity>();
            if (param != null)
            {
                if (!string.IsNullOrEmpty(param.UserName))
                {
                    expression = expression.And(t => t.UserName.Contains(param.UserName));
                }
                if (!string.IsNullOrEmpty(param.UserIds))
                {
                    long[] userIdList = TextHelper.SplitToArray<long>(param.UserIds, ',');
                    expression = expression.And(t => userIdList.Contains(t.Id.Value));
                }
                if (!string.IsNullOrEmpty(param.Mobile))
                {
                    expression = expression.And(t => t.Mobile.Contains(param.Mobile));
                }
                if (param.UserStatus > -1)
                {
                    expression = expression.And(t => t.UserStatus == param.UserStatus);
                }
                if (!string.IsNullOrEmpty(param.StartTime.ParseToString()))
                {
                    expression = expression.And(t => t.BaseModifyTime >= param.StartTime);
                }
                if (!string.IsNullOrEmpty(param.EndTime.ParseToString()))
                {
                    param.EndTime = param.EndTime.Value.Date.Add(new TimeSpan(23, 59, 59));
                    expression = expression.And(t => t.BaseModifyTime <= param.EndTime);
                }
                if (param.ChildrenDepartmentIdList != null && param.ChildrenDepartmentIdList.Count > 0)
                {
                    expression = expression.And(t => param.ChildrenDepartmentIdList.Contains(t.DepartmentId.Value));
                }
                if (param.EmpId > -1) 
                {
                    expression = expression.And(t => t.EmpId == param.EmpId);
                }
            }
            return expression;
        }
        #endregion

        #region 同步
        public async Task<TData> SyncDept()
        {
            var db = await this.BaseRepository().BeginTrans();
            try
            {
                IEnumerable<ImportDeptModel> deptList = await BaseRepository().FindList<ImportDeptModel>("select departmentNo as [order], Department_id as id,Department as depName,departtype as depType,Sup_Department as Sub_depid,Dep_Tel as depfullname from dept where sup_department not in('162','293','304','359','0','376','377','392','391','393','394','322') order by CONVERT(int,departmentNo) asc".ToString());
            
                int i = 0;
                foreach (var item in deptList)
                {
                    var entity = await BaseRepository().FindObject<ImportDeptModel>("select departmentNo as [order], Department_id as id,Department as depName,departtype as depType,Sup_Department as Sub_depid,Dep_Tel as depfullname,newid from dept where Department_Id=" + item.Sub_depid);

                    var deptEntity = new DepartmentEntity();
                    deptEntity.DepartmentName = item.depName;
                    if (item.depType == "0")
                    {
                        deptEntity.DeptType = 1;
                    }
                    else if (item.depType == "1")
                    {
                        deptEntity.DeptType = 2;
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(item.depType)&&item.depType!="NULL") 
                        {
                            deptEntity.DeptType = Convert.ToInt32(item.depType);
                        }
                    }
                    deptEntity.ParentId =entity.newid;
                    deptEntity.DepartmentSort =Convert.ToInt32(item.order);
                    deptEntity.JobIds = new List<long>();
                    await deptEntity.Create();
                    await this.BaseRepository().Insert<DepartmentEntity>(deptEntity);
                  

                    await BaseRepository().ExecuteBySql("update dept set newid='" + deptEntity.Id + "' where Department_Id=" + item.id);
                }
               
               
            }
            catch(Exception ex)
            {
                await db.RollbackTrans();
                throw;
            }
           
            return new TData();
        }
        public async Task<TData> SyncJob()
        {
            var db = await this.BaseRepository().BeginTrans();
            try
            {
                IEnumerable<ImportJobModel> deptList = await BaseRepository().FindList<ImportJobModel>("select Job_Id,Job_name,Job_state,newid from job".ToString());

                int i = 0;
                foreach (var item in deptList)
                {
                    if (item.Job_Id == "175") 
                    {
                        var ss = "";
                    }
                    try
                    {
                        var deptEntity = new JobEntity();
                        deptEntity.JobName = item.Job_name;
                        if (item.Job_state!="NULL"||string.IsNullOrEmpty(item.Job_state))
                        {
                            deptEntity.JobType = Convert.ToInt32(item.Job_state);
                        }
                        deptEntity.ParentId = 0;
                        deptEntity.JobSort = i++;
                        await deptEntity.Create();
                        await this.BaseRepository().Insert<JobEntity>(deptEntity);


                        await BaseRepository().ExecuteBySql("update job set newid='" + deptEntity.Id + "' where Job_id=" + item.Job_Id);
                    }
                    catch(Exception ex) 
                    {
                        var exce = ex;
                    }
                }

                var entity = await BaseRepository().FindList<ImportJobDeptMapping>("SELECT a.Job_id,a.Dep_id,b.newid as deptnewid,c.newid  as jobnewid from jobdeptmapp a left join dept b on a.Dep_id=b.id left join job c on a.Job_id=c.Job_id");
                foreach (var item in entity)
                {
                    var jobwithdept = new DeptWithJobEntity();
                    jobwithdept.DeptId = item.deptnewid ;
                    jobwithdept.JobId = item.jobnewid;
                    jobwithdept.Create();
                    await this.BaseRepository().Insert<DeptWithJobEntity>(jobwithdept);

                }

            }
            catch (Exception ex)
            {
                await db.RollbackTrans();
                throw;
            }

            return new TData();
        }
        public async Task<TData> SyncEmployee()
        {
            var db = await this.BaseRepository().BeginTrans();
            try
            {
                IEnumerable<ImportEmployee> EmployeeList = await BaseRepository().FindList<ImportEmployee>("select * from employee".ToString());

                int i = 0;
                foreach (var item in EmployeeList)
                {
                    var employeeModel = new EmployeeEntity();
                    employeeModel.EmpAddress = item.permanentAddress;
                   
                    employeeModel.EmpWorkID = item.employeeCardId;
                    employeeModel.EmpName = item.employeeName;
                    employeeModel.EmpDdentityNumber = item.identityCardNo;
                    // employeeModel.EmpJoinWorkTime = item.FirstWorkDate;
                    employeeModel.EmpTelePhone = item.telePhone;
                    
                    employeeModel.Create();
                    await this.BaseRepository().Insert<EmployeeEntity>(employeeModel);


                    await BaseRepository().ExecuteBySql("update employee set newid='" + employeeModel.Id + "' where id=" + item.id);
                    var partEntity = new EmployeePartTimeEntity();
                    

                }
                
                
                var entity = await BaseRepository().FindList<importpart>("SELECT b.newid as deptnewid,a.newid as empid from employee a left join dept b on a.newDepartmentid=b.id");
                foreach (var item in entity)
                {
                    var entityp = new EmployeePartTimeEntity();
                    entityp.DeptId = item.deptnewid;
                    entityp.EmpId = item.empid;
                    entityp.Create();
                    await this.BaseRepository().Insert<EmployeePartTimeEntity>(entityp);

                }

            }
            catch (Exception ex)
            {
                await db.RollbackTrans();
                throw;
            }

            return new TData();
        }
        public async Task<TData> SyncUser()
        {
            var db = await this.BaseRepository().BeginTrans();
            try
            {
                IEnumerable<importuser> userList = await BaseRepository().FindList<importuser>(@"
                                                                SELECT
	                                                                a.UserID,
	                                                                a.UserName,
	                                                                a.PASSWORD,
	                                                                a.TrueName,
	                                                                a.sex,
	                                                                a.DepartmentId,
	                                                                b.employeename,
	                                                                b.identitycardno,
	                                                                a.note_username,
	                                                                b.depid,
	                                                                b.worktypeid,
	                                                                b.residentialaddress,
	                                                                b.jjlxname,
	                                                                b.jjtelephone,
	                                                                b.lineschool,
	                                                                b.lineprofession,
	                                                                b.linejointime,
	                                                                b.lineActualPosition,
	                                                                b.linesubdepid
                                                                FROM
	                                                                [USER] a
	                                                                LEFT JOIN person b ON a.note_username = b.number AND b.isdel = 0
	                                                                ".ToString());

                foreach (var item in userList)
                {

                    var empEntity = new EmployeeEntity();
                    empEntity.EmpAddress = item.residentialaddress;
                    empEntity.EmpDdentityNumber = item.identitycardno;
                    empEntity.EmpWorkID = item.note_username;
                    empEntity.EmpName = item.TrueName;
                    empEntity.Create();

                    await this.BaseRepository().Insert<EmployeeEntity>(empEntity);



                    var userEntity = new UserEntity();
                    userEntity.UserName = item.UserName;
                    userEntity.OriginalUserId = Convert.ToInt32(item.UserID);
                    userEntity.Salt = GetPasswordSalt();
                    userEntity.Password = EncryptUserPassword("123456", userEntity.Salt);
                    //userEntity.OriginalPassword = System.Text.Encoding.Default.GetBytes(item.PASSWORD);
                    userEntity.EmpId = empEntity.Id;
                    if (!string.IsNullOrEmpty(item.UserID)) 
                    {
                        userEntity.OriginalUserId =Convert.ToInt32(item.UserID);
                    }
                    await userEntity.Create();
                    await this.BaseRepository().Insert<UserEntity>(userEntity);

                    var deptlist = await BaseRepository().FindObject<DepartmentEntity>("SELECT * from SysDepartment where OriginalDeptId='"+item.depid+"'");
                    if (deptlist != null) 
                    {
                        var partDept = new EmployeePartTimeEntity();
                        partDept.DeptId = deptlist.Id.Value;
                        partDept.Type = 1;
                        partDept.EmpId = empEntity.Id.Value;
                        partDept.Create();
                        await this.BaseRepository().Insert<EmployeePartTimeEntity>(partDept);
                    }
                  
                    
           
                }
                //int i = 0;
                //foreach (var item in EmployeeList)
                //{
                //    var user = new UserEntity();
                //    user.EmpId =long.Parse(item.EmployeeID);
                //    user.UserName = item.UserName;
                //    user.OriginalUserId = Convert.ToInt32(item.UserID);
                //    user.Salt = GetPasswordSalt();
                //    user.Password = EncryptUserPassword("123456", user.Salt);

                //    await user.Create();
                //    await this.BaseRepository().Insert<UserEntity>(user);


                //    //await BaseRepository().ExecuteBySql("update user set newid='" + user.Id + "' where UserId=" + item.UserID);
                //    var partEntity = new EmployeePartTimeEntity();


                //}




            }
            catch (Exception ex)
            {
                await db.RollbackTrans();
                throw;
            }

            return new TData();
        }
        private string EncryptUserPassword(string password, string salt)
        {
            string md5Password = SecurityHelper.MD5ToHex(password);
            string encryptPassword = SecurityHelper.MD5ToHex(md5Password.ToLower() + salt).ToLower();
            return encryptPassword;
        }

        /// <summary>
        /// 密码盐
        /// </summary>
        /// <returns></returns>
        private string GetPasswordSalt()
        {
            return new Random().Next(1, 100000).ToString();
        }

        public class importuser 
        {
            public string UserID { get; set; }
            public string UserName { get; set; }
            public string PASSWORD { get; set; }
            public string TrueName { get; set; }
            public string sex { get; set; }
            public string DepartmentId { get; set; }
            public string employeename { get; set; }
            public string identitycardno { get; set; }
            public string note_username { get; set; }
            public string depid { get; set; }
            public string worktypeid { get; set; }
            public string residentialaddress { get; set; }
            public string jjlxname { get; set; }
            public string jjtelephone { get; set; }
            public string lineschool { get; set; }
            public string lineprofession { get; set; }
            public string linejointime { get; set; }
            public string lineActualPosition { get; set; }
            public string linesubdepi { get; set; }


        }
        public class importperson 
        {
            public string id { get; set; }
            public string employeename { get; set; }
            public string sex { get; set; }
            public string identitycardno { get; set; }
            public string number { get; set; }
            public string depid { get; set; }
            public string postId { get; set; }
            public string postname { get; set; }
            public string worktypeid { get; set; }
            public string residentialaddress { get; set; }
            public string mobilePhone { get; set; }
            public string numberplate { get; set; }
            public string jjlxname { get; set; }
            public string jjtelephone { get; set; }
            public string fjname { get; set; }
            public string fjnewname { get; set; }
            public string adder { get; set; }
            public string addtime { get; set; }
            public string isdel { get; set; }
            public string lineeducation { get; set; }
            public string lineschool { get; set; }
            public string lineprofession { get; set; }
            public string linejointime { get; set; }
            public string lineActualPosition { get; set; }
            public string linestate { get; set; }
            public string linesubdepid { get; set; }

        }
        public class importtxl 
        {
            public string txlID { get; set; }
            public string txl_username { get; set; }
            public string txl_truename { get; set; }
            public string txl_userid { get; set; }
            public string txl_numbers { get; set; }
            public string txl_depID { get; set; }
            public string txl_adder { get; set; }
            public string txl_addertime { get; set; }
            public string txl_userGZZ { get; set; }
            public string txl_groupList { get; set; }
            public string txl_note { get; set; }
            public string txl_birthday { get; set; }
            public string txl_partyday { get; set; }
            public string txl_isdel { get; set; }

        }
        public class importpart 
        {
            public long deptnewid { get; set; }
            public long empid { get; set; }
        }
        public class ImportDeptModel 
        {
            public string id { get; set; }
            public string depName { get; set; }
            public string depfullname { get; set; }
            public string depType { get; set; }
            public string Sub_depid { get; set; }
            public string order { get; set; }
            public long newid { get; set; }
        }
        public class ImportJobModel 
        {
            public string Job_Id { get; set; }
            public string Job_name { get; set; }
            public string Job_state { get; set; }
            public long newid { get; set; }
        }
        public class ImportJobDeptMapping  
        {
            public long Dep_id { get; set; }
            public long Job_id { get; set; }
            public long deptnewid { get; set; }
            public long jobnewid { get; set; }
        }
        public class ImportEmployee 
        {
            public string id { get; set; }
            public string employeeId { get; set; }
            public string employeeCardId { get; set; }
            public string workType { get; set; }
            public string employeeName { get; set; }
            public string identityCardNo { get; set; }
            public string sex { get; set; }
            public string birthday { get; set; }
            public string notion { get; set; }
            public string habitAddress { get; set; }
            public string permanentAddress { get; set; }
            public string politicsStatus { get; set; }
            public string FirstWorkDate { get; set; }
            public string gzdfWorkAge { get; set; }
            public string comeWorkDate { get; set; }
            public string postCode { get; set; }
            public string telePhone { get; set; }
            public string mobilePhone { get; set; }
            public string fySoldier { get; set; }
            public string labourNature { get; set; }
            public string unitSource { get; set; }
            public string note { get; set; }
            public string financeSplit { get; set; }
            public string status { get; set; }
            public string gdDate { get; set; }
            public string personType { get; set; }
            public string ydw { get; set; }
            public string cwcfzg { get; set; }
            public string cwcfxmgscf { get; set; }
            public string lzxz { get; set; }
            public string csDate { get; set; }
            public string jrDate { get; set; }
            public string jfmonth { get; set; }
            public string newDepartmentid { get; set; }
            public string Newjobid { get; set; }
            public string IsFuBingYiNew { get; set; }
            public string XueLiNew { get; set; }
            public string PaiZhuanZheng { get; set; }
            public string JiNengDengJi { get; set; }
            public string ZhuanYeJiShuZhiWu { get; set; }
            public string SuoShuJieDao { get; set; }
            public string GangWeiXuLie { get; set; }
            public string RenYuanZhuangTai { get; set; }
            public string RenYuanShenFen { get; set; }
            public string ZhiWuJiBie { get; set; }
            public string SuoShuXianLu { get; set; }
            public string ispublic { get; set; }
            public long newid { get; set; }

        }
        #endregion
    }
}
