﻿using System;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Linq.Expressions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Util;
using Util.Extension;
using Util.Model;
using Data;
using Data.Repository;
using Entity.OrganizationManage;
using Model.Param.OrganizationManage;

namespace Service.OrganizationManage
{
    public class EmployeePartTimeService: RepositoryFactory
    {
        #region 提交
        public async Task SaveEmployeePartTimeForm(EmployeePartTimeEntity entity)
        {
            if (entity.Id.IsNullOrZero())
            {
                entity.Create();
                await this.BaseRepository().Insert(entity);
            }
            else
            {

                await this.BaseRepository().Update(entity);
            }
        }
        public bool ExistPartTime(EmployeePartTimeEntity entity)
        {
            var expression = LinqExtensions.True<EmployeePartTimeEntity>();
            if (!entity.Id.IsNullOrZero())
            {
                //主部门-修改员工信息时触发的
                expression = expression.And(c => c.EmpId == entity.EmpId && c.DeptId == entity.DeptId &&c.JobId==entity.JobId&&c.Id!=entity.Id);
            }
            else
            {
                expression = expression.And(c => c.EmpId == entity.EmpId && c.DeptId == entity.DeptId && c.JobId == entity.JobId);
            }
            return this.BaseRepository().IQueryable(expression).Count() > 0 ? true : false;
        
        }
        public async Task DeleteForm(string ids)
        {
            long[] idArr = TextHelper.SplitToArray<long>(ids, ',');
            await this.BaseRepository().Delete<EmployeeEntity>(idArr);
        }
        #endregion
        
        public async Task<List<EmployeePartTimeListVM>> GetEmpPartTimeList(long empId)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append(@"SELECT
	                        SEPT.Id,
	                        DEP.DepartmentName AS DeptName,
	                        JOB.JobName,
	                        SEPT.Type AS PartType 
                        FROM
	                        SysEmployeePartTime SEPT
	                        LEFT JOIN SysDepartment DEP ON SEPT.DeptId= DEP.Id
	                        LEFT JOIN SysJob JOB ON SEPT.JobId= JOB.Id 
                        WHERE
	                        SEPT.EmpId= '"+empId+"'");
            IEnumerable<EmployeePartTimeListVM> partTimeList = await this.BaseRepository().FindList<EmployeePartTimeListVM>(strSql.ToString());
            foreach (var item in partTimeList)
            {
                item.PartType = item.PartType == "1" ? "主部门岗位" : "兼职部门岗位";
            }
            return partTimeList.ToList();
        }
        public async Task<EmployeePartTimeEntity> GetEntity(long empid,long id)
        {
            if (!empid.IsNullOrZero()) 
            {
                return await this.BaseRepository().FindEntity<EmployeePartTimeEntity>(c=>c.EmpId==empid&&c.Type==1);
            }
            return await this.BaseRepository().FindEntity<EmployeePartTimeEntity>(id);
        }
    }
}
