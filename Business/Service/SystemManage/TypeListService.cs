﻿using System;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Linq.Expressions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Util;
using Util.Extension;
using Util.Model;
using Data;
using Data.Repository;
using Entity.SystemManage;
using Model.Param.SystemManage;

namespace Service.SystemManage
{
    /// <summary>
    /// 创 建：admin
    /// 日 期：2021-06-29 11:23
    /// 描 述：服务类
    /// </summary>
    public class TypeListService :  RepositoryFactory
    {
        #region 获取数据
        public async Task<List<TypeListEntity>> GetList(TypeListListParam param)
        {
            var expression = ListFilter(param);
            var list = await this.BaseRepository().FindList(expression);
            return list.ToList();
        }

        public async Task<List<TypeListEntity>> GetPageList(TypeListListParam param, Pagination pagination)
        {
            var expression = ListFilter(param);
            var list= await this.BaseRepository().FindList(expression, pagination);
            return list.ToList();
        }

        public async Task<TypeListEntity> GetEntity(long id)
        {
            return await this.BaseRepository().FindEntity<TypeListEntity>(id);
        }
        #endregion

        #region 提交数据
        public async Task SaveForm(TypeListEntity entity)
        {
            if (entity.Id.IsNullOrZero())
            {
                await entity.Create();
                await this.BaseRepository().Insert(entity);
            }
            else
            {
                await entity.Modify();
                await this.BaseRepository().Update(entity);
            }
        }

        public async Task DeleteForm(string ids)
        {
            long[] idArr = TextHelper.SplitToArray<long>(ids, ',');
            await this.BaseRepository().Delete<TypeListEntity>(idArr);
        }
        #endregion

        #region 私有方法
        private Expression<Func<TypeListEntity, bool>> ListFilter(TypeListListParam param)
        {
            var expression = LinqExtensions.True<TypeListEntity>();
            if (param != null)
            {
            }
            return expression;
        }
        #endregion
    }
}
