﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Linq.Expressions;
using System.Data;
using System.Data.Common;
using Data.Repository;
using Entity.OrganizationManage;
using Enum.OrganizationManage;
using Model.Param.OrganizationManage;
using Util;
using Util.Model;
using Util.Extension;
using Enum;
using Entity;
using Data.EF;
using Service.SystemManage;
using Entity.SystemManage;
namespace Service
{
    
    public class SyncDataService : RepositoryFactory
    {
        #region 同步MetroTypeList
        public async Task<TData> sync_metro_typelist()
        {
            var db = await this.BaseRepository().BeginTrans();
            try
            {
                IEnumerable<MetroTypeListImportModel> list = await BaseRepository().FindList<MetroTypeListImportModel>("select id as oid,upid,code,typename,[orderby] from typelist".ToString());

                int i = 0;
                foreach (var item in list)
                {
                    var entity = new TypeListEntity();
                    entity.Code = item.code;
                    entity.Name = item.typename;
                    if (item.upid == "NULL") 
                    {
                        item.upid = "78";
                    }
                    entity.ParentId = long.Parse(item.upid);
                    entity.Orderby = item.orderby;
                    entity.OriginalId = Convert.ToInt32(item.oid);
                    await entity.Create();
                    await this.BaseRepository().Insert<TypeListEntity>(entity);
                }
                var listn = await BaseRepository().FindList<TypeListEntity>("select * from SysTypeList where ParentId!=0");
                foreach (var item in listn)
                {
                    var data = await BaseRepository().FindObject<TypeListEntity>("select * from SysTypeList where OriginalId='" + item.ParentId+"'");
                    if (data != null)
                    {
                        item.ParentId = data.Id;
                    }
                    await this.BaseRepository().Update<TypeListEntity>(item);
                }
            }
            catch (Exception ex)
            {
                await db.RollbackTrans();
                throw;
            }
            return new TData();
        }
        //Metro_TypeList 数据模型-旧数据
        public class MetroTypeListImportModel
        {
            public string oid { get; set; }
            public string upid { get; set; }
            public string code { get; set; }
            public string typename { get; set; }
            public string orderby { get; set; }
            public string note { get; set; }
            public string addtime { get; set; }

        }
        #endregion
    }
}

