using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Linq.Expressions;
using System.Data;
using System.Data.Common;
using Data.Repository;
using Entity.OrganizationManage;
using Enum.OrganizationManage;
using Model.Param.OrganizationManage;
using Util;
using Util.Model;
using Util.Extension;
using Enum;
using Entity;
using Data.EF;
using Service.SystemManage;
using Entity.SystemManage;

namespace TestUnit
{
    [TestClass]
    public class UnitTest1 : RepositoryFactory
    {
 
        public async Task<TData> sync_metro_typelist()
        {
            var db = await this.BaseRepository().BeginTrans();
            try
            {
                IEnumerable<MetroTypeListImportModel> list = await BaseRepository().FindList<MetroTypeListImportModel>("select * from typelist".ToString());

                int i = 0;
                foreach (var item in list)
                {
                    var entity = new TypeListEntity();
                    entity.Code = item.code;
                    entity.Name = item.typename;
                    entity.ParentId =long.Parse(item.upid);
                    entity.Orderby = item.orderby;
                    entity.OriginalId =Convert.ToInt32(item.id);
                    await entity.Create();
                    await this.BaseRepository().Insert<TypeListEntity>(entity);
                }
                var listn = await BaseRepository().FindList<TypeListEntity>("select * from SysTypeList");
                foreach (var item in listn)
                {
                    item.ParentId = listn.ToList().FirstOrDefault(c => c.OriginalId == item.ParentId).Id;
                    await this.BaseRepository().Update<TypeListEntity>(item);
                }
            }
            catch (Exception ex)
            {
                await db.RollbackTrans();
                throw;
            }
            return new TData();
        }
        [TestMethod]
        public void testa() 
        {
            sync_metro_typelist();
        }
        //Metro_TypeList 数据模型-旧数据
        public class MetroTypeListImportModel 
        {
            public string id { get; set; }
            public string upid { get; set; }
            public string code { get; set; }
            public string typename { get; set; }
            public string orderby { get; set; }
            public string note { get; set; }
            public string addtime { get; set; }

        }
    }
}
