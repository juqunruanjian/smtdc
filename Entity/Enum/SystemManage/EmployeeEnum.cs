﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Enum
{
    /// <summary>
    /// 学历
    /// </summary>
    public enum EmpEduEnum
    {
        [Description("初中")]
        初中 = 1,

        [Description("高中")]
        高中 = 2,

        [Description("专科")]
        专科 = 3,
        [Description("本科")]
        本科 = 4,
        [Description("硕士")]
        硕士 = 5,
        [Description("博士")]
        博士 = 6,
    }
    public enum EmpStatus 
    {
        [Description("在职")]
        在职 = 1,

        [Description("离职")]
        离职 = 2,

        [Description("借调")]
        借调 = 3
    }
}
