﻿using System;
using System.Collections.Generic;

namespace Model.Param.SystemManage
{
    public class AutoJobListParam
    {
        public int? JobStatus { get; set; }

        public string JobName { get; set; }
    }
}
