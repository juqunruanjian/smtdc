﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Util;

namespace Model.Param.OrganizationManage
{
    /// <summary>
    /// 创 建：admin
    /// 日 期：2021-05-17 16:19
    /// 描 述：员工信息实体查询类
    /// </summary>
    public class EmployeeListParam
    {
        public long? DeptId { get; set; }
        public long? JobId { get; set; }
        public string EmployeeName { get; set; }
        public string EmpWorkID { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string EmpOldWorkID { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string EmpEmail { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string EmpTelePhone { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string EmpFax { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string EmpBirthplace { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string EmpNationality { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string EmpPlaceOfBirth { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string EmpAddress { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string EmpPoliticalStatus { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public int? EmpType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [JsonConverter(typeof(DateTimeJsonConverter))]
        public DateTime? EmpJoinYearMonth { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public int? EmpPosition { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [JsonConverter(typeof(DateTimeJsonConverter))]
        public DateTime? EmpIdentitydate { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string EmpEducation { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string EmpDegree { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string EmpBankCardNumber { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string EmpProvidentFundAccount { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string EmpMaritalStatus { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string EmpZipCode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string EmpMainAccountLocation { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [JsonConverter(typeof(DateTimeJsonConverter))]
        public DateTime? EmpJoinWorkTime { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [JsonConverter(typeof(DateTimeJsonConverter))]
        public DateTime? EmpJoinCompanyYear { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string EmpHomePhone { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string EmpLanguageLevel { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string EmpSocialSecurityMonth { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string EmpNote { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public int? EmpSortNo { get; set; }
        /// <summary>
        /// 0 正常 1离职 2借调
        /// </summary>
        /// <returns></returns>
        public int? EmpState { get; set; }
    }
    public class EmployeePartTimeListVM
    {
        [JsonConverter(typeof(StringJsonConverter))]
        public long Id { get; set; }
        public string DeptName { get; set; }
        public string JobName { get; set; }
        public string PartType { get; set; }
    }
}
