﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Util;

namespace Model.Param.OrganizationManage
{
    /// <summary>
    /// 创 建：admin
    /// 日 期：2021-05-18 14:26
    /// 描 述：岗位信息实体查询类
    /// </summary>
    public class JobListParam
    {
        public string Ids { get; set; }
        public string JobName { get; set; }
        public long? DepId { get; set; }
        public int JobType { get; set; }
    }
}
