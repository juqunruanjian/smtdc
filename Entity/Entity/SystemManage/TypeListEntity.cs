﻿using System;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations.Schema;
using Util;

namespace Entity.SystemManage
{
    /// <summary>
    /// 创 建：admin
    /// 日 期：2021-06-29 11:23
    /// 描 述：实体类
    /// </summary>
    [Table("SysTypeList")]
    public class TypeListEntity : BaseExtensionEntity
    {
        /// <summary>
        /// 父级
        /// </summary>
        /// <returns></returns>
        [JsonConverter(typeof(StringJsonConverter))]
        public long? ParentId { get; set; }
        /// <summary>
        /// 编码
        /// </summary>
        /// <returns></returns>
        public string Code { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        /// <returns></returns>
        public string Name { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
        /// <returns></returns>
        public string Orderby { get; set; }
        /// <summary>
        /// 模块标识
        /// </summary>
        /// <returns></returns>
        public string Module { get; set; }
        public int OriginalId { get; set; }
    }
}
