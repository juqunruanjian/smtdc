﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Util;

namespace Entity.OrganizationManage
{
    [Table("SysDeptWithJob")]
    public class DeptWithJobEntity: BaseEntity
    {
        [JsonConverter(typeof(StringJsonConverter))]
        public long DeptId { get; set; }
        [JsonConverter(typeof(StringJsonConverter))]
        public long JobId { get; set; }
        [NotMapped]
        public string DeptIds { get; set; }
        [NotMapped]
        public string JobIds { get; set; }
    }
}