﻿using System;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations.Schema;
using Util;

namespace Entity.OrganizationManage
{
    /// <summary>
    /// 创 建：admin
    /// 日 期：2021-05-17 16:19
    /// 描 述：员工信息实体类
    /// </summary>
    [Table("SysEmployee")]
    public class EmployeeEntity : BaseEntity
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string EmpName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string EmpUsedName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public int? EmpSex { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [JsonConverter(typeof(DateTimeJsonConverter))]
        public DateTime? EmpBirthday { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string EmpDdentityNumber { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string EmpWorkID { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string EmpOldWorkID { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string EmpEmail { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string EmpTelePhone { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string EmpFax { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string EmpBirthplace { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string EmpNationality { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string EmpPlaceOfBirth { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string EmpAddress { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string EmpPoliticalStatus { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public int? EmpType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [JsonConverter(typeof(DateTimeJsonConverter))]
        public DateTime? EmpJoinYearMonth { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public int? EmpPosition { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [JsonConverter(typeof(DateTimeJsonConverter))]
        public DateTime? EmpIdentitydate { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string EmpEducation { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string EmpDegree { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string EmpBankCardNumber { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string EmpProvidentFundAccount { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string EmpMaritalStatus { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string EmpZipCode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string EmpMainAccountLocation { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [JsonConverter(typeof(DateTimeJsonConverter))]
        public DateTime? EmpJoinWorkTime { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [JsonConverter(typeof(DateTimeJsonConverter))]
        public DateTime? EmpJoinCompanyYear { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string EmpHomePhone { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string EmpLanguageLevel { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string EmpSocialSecurityMonth { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string EmpNote { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public int? EmpSortNo { get; set; }
        /// <summary>
        /// 0 正常 1离职 2借调
        /// </summary>
        /// <returns></returns>
        public int? EmpState { get; set; }
        [JsonConverter(typeof(StringJsonConverter))]
        [NotMapped]
        public long? DeptId { get; set; }
        [JsonConverter(typeof(StringJsonConverter))]
        [NotMapped]
        public long? JobId { get; set; }
        /// <summary>
        /// 是否兼职 1 主 2兼
        /// </summary>
        [NotMapped]
        public int? IsPartTime { get; set; }
    }
}
