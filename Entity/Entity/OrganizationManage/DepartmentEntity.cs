﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Util;

namespace Entity.OrganizationManage
{
    [Table("SysDepartment")]
    public class DepartmentEntity : BaseExtensionEntity
    {
        [JsonConverter(typeof(StringJsonConverter))]
        public long? ParentId { get; set; }
        public string DepartmentName { get; set; }
        public string DepartmentFullName { get; set; }
        public string Telephone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        [JsonConverter(typeof(StringJsonConverter))]
        public long? PrincipalId { get; set; }
        public int? DepartmentSort { get; set; }
        public string Remark { get; set; }
        public string DeptGroupStID { get; set; }
        public string DeptGroupStName { get; set; }
        public string DeptOperManID { get; set; }
        public string DeptOperManName { get; set; }
        public int? DeptType { get; set; }
        public int? DeptState { get; set; }
        public int? DeptIsShow { get; set; }
        /// <summary>
        /// 多个部门Id
        /// </summary>
        [NotMapped]
        public string Ids { get; set; }
        /// <summary>
        /// 负责人名称
        /// </summary>
        [NotMapped]
        public string PrincipalName { get; set; }
        /// 部门对应的菜单，页面和按钮
        /// </summary>
        [NotMapped]
        public string MenuIds { get; set; }
        [NotMapped]
        public List<long> JobIds { get; set; }
    }
}
