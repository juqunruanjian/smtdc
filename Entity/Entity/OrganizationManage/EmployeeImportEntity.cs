﻿using System;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations.Schema;
using Util;
using System.ComponentModel;

namespace Entity.OrganizationManage
{
    public class EmployeeImportEntity : BaseEntity
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Description("姓名")]
        public string EmpName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Description("工号")]
        public string EmpWorkID { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        /// 
        [Description("联系方式")]
        public string EmpTelePhone { get; set; }
        [Description("身份证")]
        public string EmpDdentityNumber { get; set; }
        [Description("父级部门")]
        public string ParentDepartment { get; set; }
        [Description("所属部门")]
        public string ChildDepartment { get; set; }
        [Description("岗位")]
        public string Job { get; set; }
    }
}
