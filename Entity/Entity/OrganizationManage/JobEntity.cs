﻿using System;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations.Schema;
using Util;

namespace Entity.OrganizationManage
{
    /// <summary>
    /// 创 建：admin
    /// 日 期：2021-05-18 14:26
    /// 描 述：岗位信息实体类
    /// </summary>
    [Table("SysJob")]
    public class JobEntity : BaseExtensionEntity
    {
        /// <summary>
        /// 父部门Id(0表示是根部门)
        /// </summary>
        /// <returns></returns>
        [JsonConverter(typeof(StringJsonConverter))]
        public long? ParentId { get; set; }
        /// <summary>
        /// 部门名称
        /// </summary>
        /// <returns></returns>
        public string JobName { get; set; }
        /// <summary>
        /// 部门负责人Id
        /// </summary>
        /// <returns></returns>
        [JsonConverter(typeof(StringJsonConverter))]
        public long? PrincipalId { get; set; }
        /// <summary>
        /// 部门排序
        /// </summary>
        /// <returns></returns>
        public int? JobSort { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        /// <returns></returns>
        public string Remark { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [JsonConverter(typeof(StringJsonConverter))]
        public long? JobDeptID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public int? JobLevel { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public int? JobType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public int? JobState { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool? JobtIsShow { get; set; }
        /// <summary>
        /// 多个部门Id
        /// </summary>
        [NotMapped]
        public string Ids { get; set; }
        /// <summary>
        /// 负责人名称
        /// </summary>
        [NotMapped]
        public string PrincipalName { get; set; }
        [NotMapped]
        public string DeptName { get; set; }
        /// 岗位对应的菜单，页面和按钮
        /// </summary>
        [NotMapped]
        public string MenuIds { get; set; }
    }
}
