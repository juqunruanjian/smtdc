﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Web;
using Microsoft.AspNetCore.Mvc;
using Util;
using Util.Model;
using Entity;
using Model;
using Admin.Web.Controllers;
using Entity.SystemManage;
using Business.SystemManage;
using Model.Param.SystemManage;
using Model.Result;

namespace Admin.Web.Areas.SystemManage.Controllers
{
    /// <summary>
    /// 创 建：admin
    /// 日 期：2021-06-29 11:23
    /// 描 述：控制器类
    /// </summary>
    [Area("SystemManage")]
    public class TypeListController :  BaseController
    {
        private TypeListBLL typeListBLL = new TypeListBLL();

        #region 视图功能
        [AuthorizeFilter("system:typelist:view")]
        public ActionResult TypeListIndex()
        {
            return View();
        }

        public ActionResult TypeListForm()
        {
            return View();
        }
        #endregion

        #region 获取数据
        [HttpGet]
        [AuthorizeFilter("system:typelist:search")]
        public async Task<ActionResult> GetListJson(TypeListListParam param)
        {
            TData<List<TypeListEntity>> obj = await typeListBLL.GetList(param);
            return Json(obj);
        }
        [HttpGet]
        [AuthorizeFilter("system:typelist:search")]
        public async Task<IActionResult> GetTypeTreeListJson(TypeListListParam param)
        {
            TData<List<ZtreeInfo>> obj = await typeListBLL.GetZtreeTypeList(param);
            return Json(obj);
        }
        [HttpGet]
        [AuthorizeFilter("system:typelist:search")]
        public async Task<ActionResult> GetPageListJson(TypeListListParam param, Pagination pagination)
        {
            TData<List<TypeListEntity>> obj = await typeListBLL.GetPageList(param, pagination);
            return Json(obj);
        }

        [HttpGet]
        public async Task<ActionResult> GetFormJson(long id)
        {
            TData<TypeListEntity> obj = await typeListBLL.GetEntity(id);
            return Json(obj);
        }
        #endregion

        #region 提交数据
        [HttpPost]
        [AuthorizeFilter("system:typelist:add,system:typelist:edit")]
        public async Task<ActionResult> SaveFormJson(TypeListEntity entity)
        {
            TData<string> obj = await typeListBLL.SaveForm(entity);
            return Json(obj);
        }

        [HttpPost]
        [AuthorizeFilter("system:typelist:delete")]
        public async Task<ActionResult> DeleteFormJson(string ids)
        {
            TData obj = await typeListBLL.DeleteForm(ids);
            return Json(obj);
        }
        #endregion
    }
}
