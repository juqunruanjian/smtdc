﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Web;
using Microsoft.AspNetCore.Mvc;
using Util;
using Util.Model;
using Entity;
using Model;
using Admin.Web.Controllers;
using Entity.OrganizationManage;
using Business.OrganizationManage;
using Model.Param.OrganizationManage;
using Model.Result;
using Model.Param;

namespace Admin.Web.Areas.OrganizationManage.Controllers
{
    /// <summary>
    /// 创 建：admin
    /// 日 期：2021-05-17 16:19
    /// 描 述：员工信息控制器类
    /// </summary>
    [Area("OrganizationManage")]
    public class EmployeeController :  BaseController
    {
        private EmployeeBLL employeeBLL = new EmployeeBLL();
        private JobBLL jobBLL = new JobBLL();
        private DepartmentBLL departmentBLL = new DepartmentBLL();
        #region 视图功能
        [AuthorizeFilter("organization:employee:view")]
        public ActionResult EmployeeIndex()
        {
            return View();
        }

        public ActionResult EmployeeForm()
        {
            return View();
        }
        public ActionResult EmployeeSetting()
        {
            return View();
        }
        public ActionResult EmployeeImport() 
        {
            return View();
        }
        public ActionResult EmployeeAccountSetting()
        {
            return View();
        }
        [HttpGet]
        [AuthorizeFilter("organization:job:search,organization:user:search")]
        public async Task<IActionResult> GetJobTreeListJson(long depId)
        {
            TData<List<ZtreeInfo>> obj = await jobBLL.GetListByDept(depId);
            return Json(obj);
        }
        [HttpGet]
        [AuthorizeFilter("organization:department:search,organization:user:search")]
        public async Task<IActionResult> GetDepartmentTreeListJson(DepartmentListParam param)
        {
            TData<List<ZtreeInfo>> obj = await departmentBLL.GetZtreeDepartmentList(param);
            return Json(obj);
        }
        #endregion

        #region 获取数据
        //[HttpGet]
        //[AuthorizeFilter("organization:employee:search")]
        //public async Task<ActionResult> GetListJson(EmployeeListParam param)
        //{
        //    TData<List<EmployeeEntity>> obj = await employeeBLL.GetList(param);
        //    return Json(obj);
        //}

        [HttpGet]
        [AuthorizeFilter("organization:employee:search")]
        public async Task<ActionResult> GetPageListJson(EmployeeListParam param, Pagination pagination)
        {
            TData<List<EmployeeEntity>> obj = await employeeBLL.GetPageList(param, pagination);
            return Json(obj);
        }

        [HttpGet]
        public async Task<ActionResult> GetFormJson(long id)
        {
            TData<EmployeeEntity> obj = await employeeBLL.GetEntity(id);
            return Json(obj);
        }
        [HttpGet]
        public async Task<ActionResult> GetPartTimeFormJson(long id)
        {
            TData<EmployeePartTimeEntity> obj = await employeeBLL.GetPartTimeEntity(id);
            return Json(obj);
        }
        [HttpGet]
        public async Task<ActionResult> GetPartTimeList(long id) 
        {
            TData<List<EmployeePartTimeListVM>> obj = await employeeBLL.GetEmpPartTimeList(id);
            return Json(obj);
        }
        [HttpGet]
        public async Task<IActionResult> GetZtreeDeptJobList(DepartmentListParam param)
        {
            TData<List<ZtreeInfo>> obj = await departmentBLL.GetZtreeDeptJobList(param);
            return Json(obj);
        }
        #endregion

        #region 提交数据
        [HttpPost]
        [AuthorizeFilter("organization:employee:add,organization:employee:edit")]
        public async Task<ActionResult> SaveFormJson(EmployeeEntity entity)
        {
            TData<string> obj = await employeeBLL.SaveForm(entity);
            return Json(obj);
        }
        [HttpPost]
        [AuthorizeFilter("organization:employee:mapping")]
        public async Task<ActionResult> SavePartTimeFormJson(EmployeePartTimeEntity entity)
        {
            TData<string> obj = await employeeBLL.SavePartTimeForm(entity);
            return Json(obj);
        }
        [HttpPost]
        [AuthorizeFilter("organization:employee:delete")]
        public async Task<ActionResult> DeleteFormJson(string ids)
        {
            TData obj = await employeeBLL.DeleteForm(ids);
            return Json(obj);
        }
        //[HttpPost]
        //public async Task<IActionResult> ImportUserJson(ImportParam param)
        //{
        //    //List<UserEntity> list = new ExcelHelper<UserEntity>().ImportFromExcel(param.FilePath);
        //    //TData obj = await employeeBLL.ImportUser(param, list);
        //    //return Json(obj);
        //}

        #endregion
    }
}
