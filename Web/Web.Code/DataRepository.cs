﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Repository;
using Enum.OrganizationManage;
using Util;
using Util.Extension;

namespace Web.Code
{
    public class DataRepository : RepositoryFactory
    {
        public async Task<OperatorInfo> GetUserByToken(string token)
        {
            //if (!SecurityHelper.IsSafeSqlParam(token))
            //{
            //    return null;
            //}
            token = token.ParseToString().Trim();

            var strSql = new StringBuilder();
            strSql.Append(@"SELECT  a.Id as UserId,
                                    a.EmpId,
                                    a.UserStatus,
                                    a.IsOnline,
                                    a.UserName,
                                    a.RealName,
                                    a.Portrait,
                                    a.DepartmentId,
                                    a.JobId,
                                    a.WebToken,
                                    a.ApiToken,
                                    a.IsSystem
                            FROM    SysUser a
                            WHERE   WebToken = '" + token + "' or ApiToken = '" + token + "'  ");
            var operatorInfo = await BaseRepository().FindObject<OperatorInfo>(strSql.ToString());
            if (operatorInfo != null)
            {
                #region 角色
                strSql.Clear();
                strSql.Append(@"SELECT  a.BelongId as RoleId,RoleName
                                FROM    SysUserBelong a LEFT JOIN SysRole sr on a.BelongId=sr.id
                                WHERE   a.ObjectId = " + operatorInfo.UserId + " AND ");
                strSql.Append("         a.BelongType = " + UserBelongTypeEnum.Role.ParseToInt());
                IEnumerable<RoleInfo> roleList = await BaseRepository().FindList<RoleInfo>(strSql.ToString());
                //operatorInfo.RoleIds = string.Join(",", roleList.Select(p => p.RoleId).ToArray());
                operatorInfo.RoleInfo = roleList.ToList();
                #endregion
                if (operatorInfo.EmpId.HasValue) 
                { 
                    #region 多部门
                    strSql.Clear();
                    strSql.Append(@"SELECT  a.DeptId,Type as PartTimeType,dept.DepartmentName as DeptName
                                    FROM    SysEmployeePartTime a LEFT JOIN SysDepartment dept on a.DeptId=dept.Id
                                    WHERE   a.EmpId = " + operatorInfo.EmpId + " ");
                    IEnumerable<DepInfo> deptList = await BaseRepository().FindList<DepInfo>(strSql.ToString());
                    operatorInfo.DeptInfo = deptList.ToList();
                    #endregion
                    #region 多部门
                    strSql.Clear();
                    strSql.Append(@"SELECT  a.JobId,Type as PartTimeType,sj.JobName
                                    FROM    SysEmployeePartTime a LEFT JOIN SysJob sj on a.JobId=sj.Id
                                    WHERE   a.EmpId = " + operatorInfo.EmpId + " ");
                    IEnumerable<JobInfo> jobList = await BaseRepository().FindList<JobInfo>(strSql.ToString());
                    operatorInfo.JobInfo = jobList.ToList();
                    #endregion
                }
            }
            return operatorInfo;
        }

    }
}
