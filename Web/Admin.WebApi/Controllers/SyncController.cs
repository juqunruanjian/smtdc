﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Business.OrganizationManage;
using Entity.OrganizationManage;
using Enum;
using Model.Result.SystemManage;
using Util;
using Util.Model;
using Web.Code;
using Business;

using Data.Repository;

namespace Admin.WebApi.Controllers
{
    [Route("[controller]/[action]")]
    [ApiController]
    [AuthorizeFilter]
    public class SyncController : ControllerBase
    {
        private  SyncDataBll syncDataBll = new SyncDataBll();
        [HttpGet]
        public TData syncMetro_TypeList()
        {
            var list = syncDataBll.syncMetroTypeList();
            return new TData();
        }
    }
}